import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatSidenavModule } from '@angular/material/sidenav';
import { TweetStatViewComponent } from './components/tweet-stat-view/tweet-stat-view.component';
import { SidenavService } from './services/sidebar.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

const modules = [
  AppRoutingModule,
  BrowserModule,
  HttpClientModule,
  BrowserAnimationsModule,
  MatToolbarModule,
  MatSidenavModule,
  MatButtonModule,
  MatIconModule,
  MatDividerModule,
  MatFormFieldModule,
  MatSelectModule,
  FormsModule,
]


@NgModule({
  declarations: [AppComponent, TweetStatViewComponent],
  imports: [...modules],
  exports: [...modules],
  providers: [TweetStatViewComponent, SidenavService],
  bootstrap: [AppComponent]
})
export class AppModule { }
