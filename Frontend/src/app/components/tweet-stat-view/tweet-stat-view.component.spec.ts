import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TweetStatViewComponent } from './tweet-stat-view.component';

describe('TweetStatViewComponent', () => {
  let component: TweetStatViewComponent;
  let fixture: ComponentFixture<TweetStatViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TweetStatViewComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TweetStatViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
