import { Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { LineController, LineElement, PointElement, LinearScale, Title, CategoryScale } from 'chart.js'
import Chart from 'chart.js/auto'
import { SidenavService } from 'src/app/services/sidebar.service';
import { TweetsService } from 'src/app/services/tweets.service';

Chart.register(LineController, LineElement, PointElement, LinearScale, Title, CategoryScale);

@Component({
  selector: 'app-tweet-stat-view',
  templateUrl: './tweet-stat-view.component.html',
  styleUrls: ['./tweet-stat-view.component.css']
})

export class TweetStatViewComponent {
  selectedValue = "";
  text = "";
  
  constructor(private sidenav: SidenavService, private tweetsService: TweetsService) { }

  keyWords: any[] = [
    { value : 'nucléaire' },
    { value : 'gaz' },
    { value : 'pétrole' },
    { value : 'transition énergétique' },
    { value : 'développement durable' },
    { value : 'neutralité carbone' },
    { value : 'changement climatique' }, 
    { value : 'énergie fossile' }, 
    { value : 'éolien' },
    { value : 'hydrogène' },
    { value : 'enr' },
    { value : 'solaire' },
    { value : 'photovoltaique' },
    { value : 'transition écologique' },
    { value : 'carbone' },
    { value : 'biogaz' },
    { value : 'biomasse' },
    { value : 'hydroélectricité' },
    { value : 'énergie nouvelable' }
  ];

  toggleRightSidenav() {
    this.sidenav.toggle();
  }

  @ViewChild(MatSidenav)
  canvas: any;
  canvasPie: any;
  ctx: any;
  ctx2: any;

  @ViewChild('mychart') mychart: any;
  @ViewChild('pieChart') pieChart: any;

  ngAfterViewInit() {

    this.canvas = this.mychart.nativeElement;
    this.ctx = this.canvas.getContext('2d');

    this.mychart = new Chart(this.ctx, {
      type: 'bar',
      data: {
        labels: [],
        datasets: [{
          label: 'Nombre de Tweets',
          backgroundColor:[
            'rgb(255, 99, 132)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(75, 192, 192)',
            'rgb(54, 162, 235)',
            'rgb(153, 102, 255)',
            'rgb(201, 203, 207)'
          ],
          data: [],
        }]
      },

      options: {
        responsive: false,
        scales: {
          y: {
            title: {
              display: true,
              text: 'Nombre de Tweets',
            },
          },
        }
      },
    });

    this.canvasPie = this.pieChart.nativeElement;
    this.ctx2 = this.canvasPie.getContext('2d');

    this.pieChart = new Chart(this.ctx2, {
      type: 'doughnut',
      data: {
        labels: [
      ],
      datasets: [{
        label: 'Création du graphique',
        data: [],
        backgroundColor: [
          'rgb(255, 99, 132)',
          'rgb(255, 159, 64)',
          'rgb(255, 205, 86)',
          'rgb(75, 192, 192)',
          'rgb(54, 162, 235)',
          'rgb(153, 102, 255)',
          'rgb(201, 203, 207)'
        ],
        hoverOffset: 4
      }]},
      options: {
        responsive: false,
      }
    })
  }

  validate() {
    this.tweetsService.getTweetsCountFor(this.selectedValue).subscribe(data => {
      this.addData(this.mychart,data.keyword, data.total_tweet_count)
      this.addData(this.pieChart,data.keyword, data.total_tweet_count)
    })
    this.tweetsService.getLatestTweetsFor(this.selectedValue).subscribe(data => {
      this.text = data.toString()
    })
  }

  addData(chart: { data: { labels: any[]; datasets: { data: any[]; }[]; }; update: () => void; }, label: string, data: number) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset: { data: any[]; }) => {
        dataset.data.push(data);
    });
    chart.update();
  }
}
