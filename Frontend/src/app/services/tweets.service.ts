import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs/internal/Observable';

export type TweetCount = { keyword: string, total_tweet_count: number };

@Injectable({
    providedIn: 'root'
})
export class TweetsService {

    constructor(private http: HttpClient) {
    }

    uri = environment.BACKEND_ADDRESS


    getTweetsCountFor(keyword: string): Observable<TweetCount> {
        return this.http.get<TweetCount>(`${this.uri}/tweetsCount/${keyword}`)
    }
    

    getLatestTweetsFor(keyword: string) {
        return this.http.get(`${this.uri}/latestTweets/${keyword}`);
    }

}