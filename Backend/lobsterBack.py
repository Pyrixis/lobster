from flask import Flask
from flask_cors import cross_origin
import tweepy
import json

app = Flask(__name__)
bearer_token = "AAAAAAAAAAAAAAAAAAAAAJQAaAEAAAAAsCpwN6iJevm3ndj88NWNXJjKnrI%3DcJFlwRUyilyHr6wg8yPMKLC00JptNIZ3ztSJBuGDAadw9Trcng"
client = tweepy.Client(bearer_token)

@app.route('/latestTweets/<keyword>')
@cross_origin()
def index(keyword):

    tweet_serializable = []

    response_api = client.search_recent_tweets(keyword, end_time=None, expansions=None, max_results=10, media_fields=None, next_token=None,
                                           place_fields=None, poll_fields=None, since_id=None, sort_order=None, start_time=None, tweet_fields=None, until_id=None, user_fields=None)

    tweets = response_api.data

    for tweet in tweets:
        tweet_serializable.append(tweet.text)

    json_string = json.dumps(tweet_serializable)

    return json_string

@app.route('/tweetsCount/<keyword>')
@cross_origin()
def get_counts(keyword):
    response = client.get_recent_tweets_count(keyword)
    keyword_dict = {'keyword': keyword}
    final_response = merge_two_dicts(keyword_dict, response.meta)
    return final_response

def merge_two_dicts(x, y):
    z = x.copy()
    z.update(y)
    return z

app.run(debug=True)